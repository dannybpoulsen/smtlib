#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN

#include "doctest/doctest.h"
#include "smt/context.hpp"



TEST_CASE ("Model Generation Integer") {
  auto registrar = SMTLib::getSMT ("CVC4");
  REQUIRE (registrar);
  
  auto context = registrar->getFunction () () ;
  auto& builder = context->getBuilder ();

  auto intsort = builder.makeSort (SMTLib::SortKind::Integer,{});
  auto var = builder.makeVar (intsort,"test");
  auto intconst = builder.makeIntConst (10);
  auto intconst2 = builder.makeIntConst (5);
  
  auto add = builder.buildTerm (SMTLib::Ops::Plus,{intconst,var});
  auto eq = builder.buildTerm (SMTLib::Ops::Equal,{add,intconst2});

  auto solver = context->makeSolver ();
  solver->assert_formula(eq);

  REQUIRE (solver->check_sat () == SMTLib::Result::Satis);
  auto value = solver->getModelValue (var);
  CHECK (std::get<std::int64_t> (value) == static_cast<std::int64_t> (-5));

  auto boolvalue = solver->getModelValue (eq);
  CHECK (std::get<bool> (boolvalue) );
  
  
}



