
#include <iostream>
#include "smt/context.hpp"

int main () {
  auto context = SMTLib::getSMT ("CVC4")->getFunction () ();
  auto& builder = context->getBuilder ();
  auto sort = builder.makeBVSort (8);
  auto var= builder.makeVar (sort,"H");
  auto cc = builder.makeBVIntConst (64,8);
  auto comp = builder.buildTerm (SMTLib::Ops::BVComp,{var,cc});
  auto c = builder.buildTerm (SMTLib::Ops::BV2Bool, {comp});
  auto solver = context->makeSolver ();
  solver->assert_formula (c);
  std::cerr << solver->check_sat () << std::endl;
  return 1;
}
