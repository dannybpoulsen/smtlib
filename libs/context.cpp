#include <unordered_map>
#include <iostream>
#include "smt/context.hpp"

namespace SMTLib {
  auto& getMap () {
	static std::unordered_map<std::string,SMTBackendRegistrar*> map;
	return map;
  }
  
  void registerSMT (const std::string& s, SMTBackendRegistrar& smt) {
	getMap ().insert (std::make_pair (s,&smt));
  }

  SMTBackendRegistrar* getSMT (const std::string& s) {
    if (getMap ().count (s)) 
      return getMap ().at(s);
    else return nullptr;
  }
  
  
  
  std::vector<SMTBackendRegistrar*> getSMTBackends () {
    std::vector<SMTBackendRegistrar*> res;
    for (auto& it : getMap ()) {
      res.push_back (it.second);
    }
    return res;
  }
  
  

}
