#include <cvc4/cvc4.h>
#include "builder.hpp"
#include "sort.hpp"

#include "sortbuilding.hpp"
#include "termbuilding.hpp"

namespace SMTLib::CVC {
 

  ::SMTLib::Sort_ptr TermBuilder::makeBVSort (width_t w) {
    return std::make_shared<SMTLib::CVC::Sort> (manager.mkBitVectorType (w), SMTLib::SortKind::BitVector);
  }
  
  ::SMTLib::Sort_ptr TermBuilder::makeSort (SortKind kind, std::initializer_list<Sort_ptr> sorts) {
    switch (kind) {
#define X(KIND)								\
      case SMTLib::SortKind::KIND:					\
	return SortBuilder<SMTLib::SortKind::KIND>::makeSort (manager, sorts);	
      SORTS
	default:
      return nullptr;
#undef X
    }
  }
  ::SMTLib::Term_ptr TermBuilder::buildTerm (Ops op, std::initializer_list<Term_ptr> terms) {
    switch (op) {
#define X(KIND)								\
      case SMTLib::Ops::KIND:						\
	return TermBuilding<SMTLib::Ops::KIND>::buildTerm (manager, terms);	
      OPS
	default:
      return nullptr;
#undef X
    }
  }

  SMTLib::Term_ptr TermBuilder::buildTerm (Ops op, std::initializer_list<Term_ptr> terms, std::initializer_list<std::size_t> ops) {
	switch (op) {
#define X(KIND)								\
      case SMTLib::Ops::KIND:						\
	return TermBuildingIndexed<SMTLib::Ops::KIND>::buildTerm (manager, terms,ops);	
      OPS
	default:
      return nullptr;
#undef X
    }
  }
  
  Term_ptr TermBuilder::makeVar (Sort_ptr sort, const std::string name) {
    auto h = std::static_pointer_cast<SMTLib::CVC::Sort> (sort);
    std::stringstream str;
    return std::make_shared<SMTLib::CVC::Term> (SMTLib::Ops::VariableDecl,manager.mkVar (name,h->getCVC4()));
  }

  Term_ptr TermBuilder::makeIntConst (std::int64_t num) {	
	return std::make_shared<SMTLib::CVC::Term> (SMTLib::Ops::Const,manager.mkConst (::CVC4::Rational (num)));
	
  }

  Term_ptr TermBuilder::makeBVIntConst (std::uint64_t val, width_t width) {
	return std::make_shared<SMTLib::CVC::Term> (SMTLib::Ops::Const,manager.mkConst (::CVC4::BitVector (width,val)));
	
  }

  Term_ptr TermBuilder::makeBoolConst (bool val) {
	return std::make_shared<SMTLib::CVC::Term> (SMTLib::Ops::Const,manager.mkConst(val));
	
  }

  Term_ptr TermBuilder::makeStringConst (const string32& s) {
	std::vector<unsigned> init;
	for (auto& c : s) {
	  init.push_back (c);
	}
	return std::make_shared<SMTLib::CVC::Term> (SMTLib::Ops::Const,manager.mkConst(::CVC4::String (init)));
	
	
  }
	
  
}
