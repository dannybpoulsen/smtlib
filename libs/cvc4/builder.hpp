#ifndef _CVC_BUILDER__
#define _CVC_BUILDER__


#include <cvc4/cvc4.h>
#include "smt/builder.hpp"

namespace SMTLib::CVC {
  class TermBuilder : public SMTLib::TermBuilder {
  public:
    TermBuilder (CVC4::ExprManager& man) : manager(man) {}
    Sort_ptr makeBVSort (width_t);
    Sort_ptr makeSort (SortKind, std::initializer_list<Sort_ptr> sorts);
    Term_ptr buildTerm (Ops, std::initializer_list<Term_ptr> terms);
    Term_ptr buildTerm (Ops, std::initializer_list<Term_ptr> terms, std::initializer_list<std::size_t>);    
    Term_ptr makeVar (Sort_ptr sort, const std::string name);
    Term_ptr makeIntConst (std::int64_t);
    Term_ptr makeBVIntConst (std::uint64_t, width_t width);
    Term_ptr makeBoolConst (bool);
    virtual Term_ptr makeStringConst (const string32&);
	
  private:
    CVC4::ExprManager& manager;

  };
}

#endif
