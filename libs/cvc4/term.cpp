#include <cvc4/cvc4.h>
#include "term.hpp"

namespace SMTLib::CVC {
  std::size_t Term::hash() const  {
    return CVC4::ExprHashFunction{} (term);
  }

  bool Term::operator== (const ::SMTLib::Term& term) {
    assert(typeid(*this) == typeid(term));
    return this->term == static_cast<const SMTLib::CVC::Term&> (term).term;
  }
}
