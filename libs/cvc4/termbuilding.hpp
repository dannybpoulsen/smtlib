#include <cvc4/cvc4.h>
#include "builder.hpp"
#include "term.hpp"
#include "smt/ops.hpp"
#include "smt/exceptions.hpp"
#include "term2cvc4.hpp"

namespace SMTLib::CVC {

  template<SMTLib::Ops,class... exprs>
  SMTLib::Term_ptr doOperation (CVC4::ExprManager&, exprs...) {
	throw Exception ("Unsupported Operation");
  }

#define X(OURS,THEIRS)							\
  template<>															\
  SMTLib::Term_ptr doOperation<SMTLib::Ops::OURS,CVC4::Expr,CVC4::Expr> (CVC4::ExprManager& manager, CVC4::Expr l, CVC4::Expr r) { \
	static_assert (is_binary<SMTLib::Ops::OURS>::value);				\
    return std::make_shared<SMTLib::CVC::Term> (SMTLib::Ops::OURS,manager.mkExpr(CVC4::Kind::THEIRS,l,r)); \
  }
  
  BINTERMSTRANS
#undef X

  #define X(OURS,THEIRS)							\
  template<>															\
  SMTLib::Term_ptr doOperation<SMTLib::Ops::OURS,CVC4::Expr> (CVC4::ExprManager& manager, CVC4::Expr l) { \
	static_assert (is_unary<SMTLib::Ops::OURS>::value);				\
    return std::make_shared<SMTLib::CVC::Term> (SMTLib::Ops::OURS,manager.mkExpr(CVC4::Kind::THEIRS,l)); \
  }
  
  UNTERMSTRANS
#undef X

   template<>															
  SMTLib::Term_ptr doOperation<SMTLib::Ops::BV2Bool,CVC4::Expr> (CVC4::ExprManager& manager, CVC4::Expr l) { 
	auto t  = l.getType();
	assert(t.isBitVector ());
	auto& bvtype = static_cast<CVC4::BitVectorType&> (t);
	CVC4::BitVector ff (static_cast<unsigned int> (bvtype.getSize()),static_cast<unsigned int> (0));
	auto ffExpr = manager.mkConst (ff);
	auto comp = manager.mkExpr (CVC4::Kind::EQUAL,l,ffExpr);
	auto ttBool = manager.mkConst (true);
	auto ffBool = manager.mkConst (false);
	return std::make_shared<SMTLib::CVC::Term> (SMTLib::Ops::BV2Bool,manager.mkExpr(CVC4::Kind::ITE,comp,ffBool,ttBool)); 
  }

  template<>
  SMTLib::Term_ptr doOperation<SMTLib::Ops::ITE,CVC4::Expr,CVC4::Expr,CVC4::Expr> (CVC4::ExprManager& manager, CVC4::Expr c,CVC4::Expr l,CVC4::Expr r) {
    return std::make_shared<SMTLib::CVC::Term> (SMTLib::Ops::ITE,manager.mkExpr(CVC4::Kind::ITE,c,l,r));
  }

  


  template<SMTLib::Ops op,class T = void> 
  struct TermBuilding {
    static SMTLib::Term_ptr buildTerm (CVC4::ExprManager&, std::initializer_list<SMTLib::Term_ptr>) {
	  throw Exception ("Unsupported Operation");
	}
  };

  template<>
  struct TermBuilding<SMTLib::Ops::ITE,void>
  {
    static SMTLib::Term_ptr buildTerm (CVC4::ExprManager& manager, std::initializer_list<SMTLib::Term_ptr> list) {
      auto c = std::static_pointer_cast<SMTLib::CVC::Term> (*list.begin());
	  auto l = std::static_pointer_cast<SMTLib::CVC::Term> (*(list.begin()+1));
	  auto r = std::static_pointer_cast<SMTLib::CVC::Term> (*(list.begin()+2));
      return doOperation<SMTLib::Ops::ITE,CVC4::Expr,CVC4::Expr> (manager,c->getCVC4(),l->getCVC4(),r->getCVC4());
      
    }
  };

  //Binary Ops
  template<SMTLib::Ops op>
  struct TermBuilding<op,std::enable_if_t<is_binary<op>::value>>
  {
    static SMTLib::Term_ptr buildTerm (CVC4::ExprManager& manager, std::initializer_list<SMTLib::Term_ptr> list) {
      auto l = std::static_pointer_cast<SMTLib::CVC::Term> (*list.begin());
      auto r = std::static_pointer_cast<SMTLib::CVC::Term> (*(list.begin()+1));
      return doOperation<op,CVC4::Expr,CVC4::Expr> (manager,l->getCVC4(),r->getCVC4());
      
    }
  };

   //Unary Ops
  template<SMTLib::Ops op>
  struct TermBuilding<op,std::enable_if_t<is_unary<op>::value>>
  {
    static SMTLib::Term_ptr buildTerm (CVC4::ExprManager& manager, std::initializer_list<SMTLib::Term_ptr> list) {
      auto l = std::static_pointer_cast<SMTLib::CVC::Term> (*list.begin());
      return doOperation<op,CVC4::Expr> (manager,l->getCVC4());
      
    }
  };

  template<> 
  struct TermBuilding<SMTLib::Ops::Select>
  {
    static SMTLib::Term_ptr buildTerm (CVC4::ExprManager& manager, std::initializer_list<SMTLib::Term_ptr> list) {
      assert(list.size()== 2);
      auto array  = std::static_pointer_cast<SMTLib::CVC::Term> (*list.begin())->getCVC4();
      auto index  = std::static_pointer_cast<SMTLib::CVC::Term> (*(list.begin()+1))->getCVC4();
      return std::make_shared<SMTLib::CVC::Term> (SMTLib::Ops::Select,manager.mkExpr (CVC4::Kind::SELECT,
										      array,
										      index));
    }
  };
  
    template<> 
  struct TermBuilding<SMTLib::Ops::Store>
  {
    static SMTLib::Term_ptr buildTerm (CVC4::ExprManager& manager, std::initializer_list<SMTLib::Term_ptr> list) {
      assert(list.size()== 3);
      auto array  = std::static_pointer_cast<SMTLib::CVC::Term> (*list.begin())->getCVC4();
      auto index  = std::static_pointer_cast<SMTLib::CVC::Term> (*(list.begin()+1))->getCVC4();
      auto storee  = std::static_pointer_cast<SMTLib::CVC::Term> (*(list.begin()+2))->getCVC4();
      
      return std::make_shared<SMTLib::CVC::Term> (SMTLib::Ops::Select,manager.mkExpr (CVC4::Kind::STORE,
										      array,
										      index,
										      storee
										      ));
    }
  };
  
  
  template<SMTLib::Ops op,class T = void> 
  struct TermBuildingIndexed {
    static SMTLib::Term_ptr buildTerm (CVC4::ExprManager& em, std::initializer_list<SMTLib::Term_ptr> list, std::initializer_list<std::size_t> ops) {
	  if (ops.size() == 0) {
		return TermBuilding<op>::buildTerm (em,list);
	  }
	  else {
	    throw Exception ("Unsupported Operation");
	  }
	}
  };

 
  
  template<SMTLib::Ops op> 
  struct TermBuildingIndexed<op,std::enable_if_t<op == SMTLib::Ops::ZExt>>
  {
    static SMTLib::Term_ptr buildTerm (CVC4::ExprManager& manager, std::initializer_list<SMTLib::Term_ptr> list, std::initializer_list<std::size_t> ops) {
	  assert(list.size()== 1);
	  assert(ops.size() == 1);
	  auto cval = std::static_pointer_cast<SMTLib::CVC::Term> (*list.begin())->getCVC4 ();
	  auto extend = manager.mkConst (CVC4::BitVectorZeroExtend (*ops.begin()));
	  return std::make_shared<SMTLib::CVC::Term> (SMTLib::Ops::ZExt,manager.mkExpr(CVC4::Kind::BITVECTOR_ZERO_EXTEND,extend,cval));
	}
  };

  template<SMTLib::Ops op> 
  struct TermBuildingIndexed<op,std::enable_if_t<op == SMTLib::Ops::Extract>>
  {
    static SMTLib::Term_ptr buildTerm (CVC4::ExprManager& manager, std::initializer_list<SMTLib::Term_ptr> list, std::initializer_list<std::size_t> ops) {
	  assert(list.size()== 1);
	  assert(ops.size() == 2);
	  auto extract = manager.mkConst (CVC4::BitVectorExtract (*ops.begin(),*(ops.begin()+1)));
	  auto cval = std::static_pointer_cast<SMTLib::CVC::Term> (*list.begin())->getCVC4 ();
	  return std::make_shared<SMTLib::CVC::Term> (SMTLib::Ops::Extract,manager.mkExpr(extract,cval));
	}
  };
  
  template<SMTLib::Ops op> 
  struct TermBuildingIndexed<op,std::enable_if_t<op == SMTLib::Ops::SExt>>
  {
    static SMTLib::Term_ptr buildTerm (CVC4::ExprManager& manager, std::initializer_list<SMTLib::Term_ptr> list, std::initializer_list<std::size_t> ops) {
	  assert(list.size()== 1);
	  assert(ops.size() == 1);
	  auto cval = std::static_pointer_cast<SMTLib::CVC::Term> (*list.begin())->getCVC4 ();
	  auto extend = manager.mkConst (CVC4::BitVectorSignExtend (*ops.begin()));
	  return std::make_shared<SMTLib::CVC::Term> (SMTLib::Ops::SExt,manager.mkExpr(CVC4::Kind::BITVECTOR_SIGN_EXTEND,extend,cval));
	}
  };
  
  template<> 
  struct TermBuildingIndexed<SMTLib::Ops::Bool2BV,void>
  {
    static SMTLib::Term_ptr buildTerm (CVC4::ExprManager& manager, std::initializer_list<SMTLib::Term_ptr> list, std::initializer_list<std::size_t> ops) {
	  assert(list.size()== 1);
	  assert(ops.size() == 1);
	  auto width = *ops.begin();
	  auto cval = std::static_pointer_cast<SMTLib::CVC::Term> (*list.begin())->getCVC4 ();
	  CVC4::BitVector ff (static_cast<unsigned int> (width),static_cast<unsigned int> (0));
	  CVC4::BitVector tt (static_cast<unsigned int> (width),static_cast<unsigned int> (1));
	  
	  return std::make_shared<SMTLib::CVC::Term> (SMTLib::Ops::Bool2BV,manager.mkExpr(CVC4::Kind::ITE,cval,manager.mkConst(tt),manager.mkConst(ff)));
	}
  };

  
}
