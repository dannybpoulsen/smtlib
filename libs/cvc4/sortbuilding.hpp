#include <cvc4/cvc4.h>
#include "smt/exceptions.hpp"
#include "builder.hpp"
#include "sort.hpp"


namespace SMTLib::CVC {

  template<SortKind>
  struct SortBuilder {
    static ::SMTLib::Sort_ptr makeSort (CVC4::ExprManager&, std::initializer_list<Sort_ptr>) {
	  throw Exception ("Unsupported Operation");
    }
  };

  template<>
  struct SortBuilder<SMTLib::SortKind::Bool> {
    static ::SMTLib::Sort_ptr makeSort (CVC4::ExprManager& manager, std::initializer_list<Sort_ptr> sorts) {
      assert(sorts.size () == 0);
      return std::make_shared<SMTLib::CVC::Sort> (manager.booleanType (),SMTLib::SortKind::Bool);
    }
  };

  template<>
  struct SortBuilder<SMTLib::SortKind::Integer> {
    static ::SMTLib::Sort_ptr makeSort (CVC4::ExprManager& manager, std::initializer_list<Sort_ptr> sorts) {
      assert(sorts.size () == 0);
      return std::make_shared<SMTLib::CVC::Sort> (manager.integerType (),SMTLib::SortKind::Integer);
    }
  };

  template<>
  struct SortBuilder<SMTLib::SortKind::String> {
    static ::SMTLib::Sort_ptr makeSort (CVC4::ExprManager& manager, std::initializer_list<Sort_ptr> sorts) {
      assert(sorts.size () == 0);
      return std::make_shared<SMTLib::CVC::Sort> (manager.stringType (),SMTLib::SortKind::String);
    }
  };

  template<>
  struct SortBuilder<SMTLib::SortKind::Array> {
    static ::SMTLib::Sort_ptr makeSort (CVC4::ExprManager& manager, std::initializer_list<Sort_ptr> sorts) {
      assert(sorts.size () == 2);
      auto indexT = std::static_pointer_cast<SMTLib::CVC::Sort> (*sorts.begin());
      auto constituentT = std::static_pointer_cast<SMTLib::CVC::Sort> (*(sorts.begin()+1));
      
      return std::make_shared<SMTLib::CVC::Sort> (manager.mkArrayType (indexT->getCVC4(),constituentT->getCVC4()),SMTLib::SortKind::Integer);
    }
  };
}
