#ifndef _SOLVER__
#define _SOLVER__

#include <cvc4/cvc4.h>
#include <memory>
#include <vector>

#include "smt/solver.hpp"
#include "smt/term.hpp"

namespace SMTLib::CVC {
  class Solver : public SMTLib::Solver {
  public:
    Solver (CVC4::ExprManager& em) : engine (&em) {
	  engine.setOption("produce-models", "true");
	  
	}
    virtual ~Solver () {}
    virtual void assert_formula (const SMTLib::Term_ptr& ) ;
    virtual SMTLib::Result check_sat ();
    virtual SMTLib::Result check_sat (const SMTLib::Assumptions& ) ;
    virtual SMTLib::Values getModelValue (const Term_ptr& ) const; 
  private:
    CVC4::SmtEngine engine;
  };

  
}


#endif
