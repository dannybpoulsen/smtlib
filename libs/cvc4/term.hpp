#ifndef _CVC4_TERM__
#define _CVC4_TERM__

#include <cvc4/cvc4.h>
#include "smt/term.hpp"
#include "smt/ops.hpp"
#include "smt/sort.hpp"

namespace SMTLib {
  namespace CVC {
    class Term : public ::SMTLib::Term {
    public:
      Term (SMTLib::Ops op, CVC4::Expr e) : term(e), op(op) {}
      std::size_t hash() const override ;
      SMTLib::Ops get_op() const override {return op;}
      bool operator== (const ::SMTLib::Term& term) override;
      auto getCVC4 () const {return term;}
	  virtual std::ostream& output (std::ostream& os) const {return os << term.toString ();}
	private:
      CVC4::Expr term;
      SMTLib::Ops op;
    };
  }
}

#endif
