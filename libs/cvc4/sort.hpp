#ifndef _CVC4_SORT__
#define _CVC4_SORT__

#include <cvc4/cvc4.h>
#include "sort.hpp"

namespace SMTLib {
  namespace CVC {
    class Sort : public :: SMTLib::Sort {
    public:
      Sort (CVC4::Type t, SortKind k) : type(t),kind(k)  {}
      SortKind getSortKind () const {return kind;}
      virtual bool operator== (const ::SMTLib::Sort& sort) const override  {
	auto& s = static_cast<const Sort&> (sort);
	return s.kind == this->kind &&
	  s.type == this->type;
      }

      auto getCVC4 () const {return type;}
    private:
      CVC4::Type type;
      SortKind kind;
    };
  }
}

#endif
