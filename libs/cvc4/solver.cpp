#include <sstream>

#include "term.hpp"
#include "solver.hpp"
#include "smt/exceptions.hpp"

void SMTLib::CVC::Solver::assert_formula (const SMTLib::Term_ptr& term) {
  assert(term);
  auto t = std::static_pointer_cast<SMTLib::CVC::Term> (term);
  engine.assertFormula (t->getCVC4 ());
}

SMTLib::Result SMTLib::CVC::Solver::check_sat () {
  auto res = engine.checkSat ();
  switch (res.isSat ()) {
  case ::CVC4::Result::Sat::UNSAT:
	return SMTLib::Result::NSatis;
    break;
  case ::CVC4::Result::Sat::SAT:
    return SMTLib::Result::Satis;
    break;
  case ::CVC4::Result::Sat::SAT_UNKNOWN:
  default:
    return SMTLib::Result::Unknown;
    break;
  }
}
    
SMTLib::Result SMTLib::CVC::Solver::check_sat (const SMTLib::Assumptions& ) {
  return SMTLib::Result::Unknown;    
}

SMTLib::Values SMTLib::CVC::Solver::getModelValue (const Term_ptr& term) const {
  auto t = std::static_pointer_cast<SMTLib::CVC::Term> (term);
  auto cvc4 = t->getCVC4 ();
  if (cvc4.getType ().isInteger ()) {
	std::stringstream str;
	auto value = engine.getValue (cvc4);
	assert (value.isConst ());
	
	return static_cast<std::int64_t> (value.template getConst<CVC4::Rational> ().getNumerator().getLong ());
	
	//throw SMTLib::Exception ("Can't evaluate");
	//std::cerr << str.str () << std::endl;
	//return std::stoi (str.str());
  }
  else if (cvc4.getType ().isString ()) {
        auto val = engine.getValue (cvc4);
	assert (val.isConst ());
	const CVC4::String& sval = val.template getConst<CVC4::String>  ();
	std::vector<char32_t> res;
	for (auto k : sval.getVec ())
	  res.push_back (k);
	return res;
  }

  else if (cvc4.getType ().isBoolean ()) {
       auto val = engine.getValue (cvc4);
       assert (val.isConst ());
       return val.template getConst<bool> ();
  }
  
  else if (cvc4.getType().isBitVector ()) {
    bitvector vector;
    auto val = engine.getValue (cvc4);
    assert (val.isConst ());
    auto& sval = val.template getConst<CVC4::BitVector>  ();
    for (std::size_t i = 0; i < sval.getSize (); i++) {
      vector.push_back (sval.isBitSet (i));
    }
    return vector;
  }
  throw SMTLib::Exception ("Cannot evalute value");
  
}
