#include "context.hpp"
#include "solver.hpp"


namespace SMTLib::CVC {
  Context_ptr makeContext  () {
    return std::make_shared<SMTLib::CVC::Context> ();
  }
  
  SMTLib::TermBuilder&  Context::getBuilder () {
    return builder;
  }

  SMTLib::Solver_ptr Context::makeSolver () {
    return std::make_unique<SMTLib::CVC::Solver> (manager);
  }
  
  static SMTLib::SMTBackendRegistrar cvc4reg ("CVC4",makeContext);
  
}


