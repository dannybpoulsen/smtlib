#ifndef TERM2CVC4
#define TERM2CVC4

#define BINTERMSTRANS							\
  X(And, AND)								\
  X(Or, OR)								\
  X(Xor,XOR)								\
  X(Implies,IMPLIES)							\
  X(Equal,EQUAL)							\
  X(NotEqual,DISTINCT)							\
  X(Plus,PLUS)								\
  X(Minus,MINUS)							\
  X(Div,DIVISION)							\
  X(Lt,LT)								\
  X(LEq,LEQ)								\
  X(Gt,GT)								\
  X(GEq,GEQ)								\
  X(Concat,BITVECTOR_CONCAT)						\
  X(BVAnd,BITVECTOR_AND)						\
  X(BVOr,BITVECTOR_OR)							\
  X(BVXor,BITVECTOR_XOR)						\
  X(BVComp,BITVECTOR_COMP)						\
  X(BVAdd,BITVECTOR_PLUS)						\
  X(BVSub,BITVECTOR_SUB)						\
  X(BVMul,BITVECTOR_MULT)						\
  X(BVUDiv,BITVECTOR_UDIV)						\
  X(BVSDiv,BITVECTOR_SDIV)						\
  X(BVURem,BITVECTOR_UREM)						\
  X(BVSRem,BITVECTOR_SREM)						\
  X(BVLShl,BITVECTOR_SHL)						\
  X(BVAShr,BITVECTOR_ASHR)						\
  X(BVLShr,BITVECTOR_LSHR)						\
  X(BVULt,BITVECTOR_ULT)						\
  X(BVULEq,BITVECTOR_ULE)						\
  X(BVUGt,BITVECTOR_UGT)						\
  X(BVUGEq,BITVECTOR_UGE)						\
  X(BVSLt,BITVECTOR_SLT)						\
  X(BVSLEq,BITVECTOR_SLE)						\
  X(BVSGt,BITVECTOR_SGT)						\
  X(BVSGEq,BITVECTOR_SGE)						\
  X(StrConcat,STRING_CONCAT)					\
	
#define UNTERMSTRANS							\
  X(Not,NOT)									\
  X(BVNot,BITVECTOR_NOT)						\
  X(BVNeg,BITVECTOR_NEG)						\
  
#endif
