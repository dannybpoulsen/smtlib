#ifndef _CVC_CONTEXT__
#define _CVC_CONTEXT__

#include <cvc4/cvc4.h>
#include "smt/context.hpp"
#include "builder.hpp"


namespace SMTLib::CVC {
  class Context : public SMTLib::Context {
  public:
    Context () : builder(manager) {}
    virtual SMTLib::TermBuilder& getBuilder ();
    virtual SMTLib::Solver_ptr makeSolver ();
  private:
    CVC4::ExprManager manager;
    TermBuilder builder;
  };
}


#endif
