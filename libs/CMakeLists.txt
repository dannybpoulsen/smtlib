
add_library (smtlib SHARED context.cpp)
target_sources(smtlib PUBLIC FILE_SET HEADERS
	BASE_DIRS ${CMAKE_CURRENT_SOURCE_DIR}/pubinclude
	FILES ${CMAKE_CURRENT_SOURCE_DIR}/pubinclude/smt/builder.hpp
	      ${CMAKE_CURRENT_SOURCE_DIR}/pubinclude/smt/context.hpp
	      ${CMAKE_CURRENT_SOURCE_DIR}/pubinclude/smt/exceptions.hpp
	      ${CMAKE_CURRENT_SOURCE_DIR}/pubinclude/smt/ops.hpp
	      ${CMAKE_CURRENT_SOURCE_DIR}/pubinclude/smt/solver.hpp
	      ${CMAKE_CURRENT_SOURCE_DIR}/pubinclude/smt/sort.hpp
	      ${CMAKE_CURRENT_SOURCE_DIR}/pubinclude/smt/term.hpp
	      )
	      
target_include_directories (smtlib PUBLIC $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}/pubinclude> $<INSTALL_INTERFACE:include>)

add_library(SMTLIb::lib ALIAS smtlib)


if(CVC4_FOUND)
	add_subdirectory (cvc4)
	target_link_libraries(smtlib PRIVATE -Wl,-whole-archive  cvc4 -Wl,-no-whole-archive )
endif ()

