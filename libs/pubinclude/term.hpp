#ifndef _TERM__
#define _TERM__

#include <memory>
#include "smt/ops.hpp"

namespace SMTLib {
  class Term {
  public:
    virtual ~Term () {}
    virtual std::size_t hash() const = 0;
    virtual Op get_op() const = 0;
    virtual Sort get_sort() const = 0;
    virtual std::string to_string() const = 0;
    virtual operator== (const Term& term) = 0;
  };

  using Term_ptr = std::shared_ptr<Term>;
  
}

#endif
