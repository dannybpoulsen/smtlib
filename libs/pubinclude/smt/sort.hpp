#ifndef _SORT__
#define _SORT__

#include <memory>
#include <cstdint>

namespace SMTLib {
#define SORTS					\
  X(Bool)						\
  X(Integer)					\
  X(BitVector)					\
  X(Array)						\
  X(String)						\
  
  enum class SortKind {
#define X(VAR)					\
		       VAR, 
		       SORTS
#undef X
  };

  class Sort {
  public:
    virtual SortKind getSortKind () const  =0;
    virtual bool operator== (const Sort&) const = 0;
  };

  using width_t = std::uint64_t;
  using Sort_ptr = std::shared_ptr<Sort>;
  
}

#endif
