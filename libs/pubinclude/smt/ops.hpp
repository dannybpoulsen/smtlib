#ifndef _SMT_OPS__
#define _SMT_OPS__

namespace SMTLib {
#define OPS				\
  						\
  X(And)						\
  X(Or)							\
  X(Xor)						\
  X(Not)						\
  X(Implies)					\
  X(Iff)						\
  X(ITE)						\
  X(Equal)						\
  X(NotEqual)					\
  								\
  X(Plus)						\
  X(Minus)						\
  X(Negate)						\
  X(Mult)						\
  X(Div)						\
  X(Lt)							\
  X(LEq)						\
  X(Gt)							\
  X(GEq)						\
  								\
  X(Concat)						\
  X(Extract)					\
  X(BVNot)						\
  X(BVNeg)						\
  X(BVAnd)						\
  X(BVOr)						\
  X(BVXor)						\
  X(BVComp)						\
  X(BVAdd)						\
  X(BVSub)						\
  X(BVMul)						\
  X(BVUDiv)						\
  X(BVSDiv)						\
  X(BVURem)						\
  X(BVSRem)						\
  X(BVLShl)						\
  X(BVAShr)						\
  X(BVLShr)						\
  X(BVULt)						\
  X(BVULEq)						\
  X(BVUGt)						\
  X(BVUGEq)						\
  X(BVSLt)						\
  X(BVSLEq)						\
  X(BVSGt)						\
  X(BVSGEq)						\
  X(ZExt)						\
  X(SExt)						\
  								\
  X(BV2Bool)					\
  X(Bool2BV)					\
  								\
  X(Select)						\
  X(Store)						\
  X(Const_Array)				\
  X(VariableDecl)				\
  X(Const)						\
								\
  X(StrConcat)					\
  X(StrLength)					\
  
  enum class Ops {
#define X(OP)					\
    OP,				
    OPS
#undef X
  };

  template<SMTLib::Ops,class T = void>
  struct is_binary {
    static constexpr bool value = false; 
  };
  
  template<SMTLib::Ops op>
  struct is_binary<op,std::enable_if_t<
    op == SMTLib::Ops::And ||
			op == SMTLib::Ops::Or ||
			op == SMTLib::Ops::Xor ||
			op == SMTLib::Ops::Implies ||
			op == SMTLib::Ops::Equal ||
			op == SMTLib::Ops::NotEqual ||
			op == SMTLib::Ops::Plus ||
			op == SMTLib::Ops::Minus ||
			op == SMTLib::Ops::Mult ||
			op == SMTLib::Ops::Div ||
			op == SMTLib::Ops::Lt ||
			op == SMTLib::Ops::LEq ||
			op == SMTLib::Ops::Gt ||
			op == SMTLib::Ops::GEq ||
			op == SMTLib::Ops::BVAnd ||
			op == SMTLib::Ops::BVOr ||
			op == SMTLib::Ops::BVXor ||
			op == SMTLib::Ops::BVAdd ||
			op == SMTLib::Ops::BVSub ||
			op == SMTLib::Ops::BVMul ||
			op == SMTLib::Ops::BVUDiv ||
			op == SMTLib::Ops::BVSDiv ||
			op == SMTLib::Ops::BVURem ||
			op == SMTLib::Ops::BVSRem ||
			op == SMTLib::Ops::BVLShl ||
    op == SMTLib::Ops::BVAShr ||
    op == SMTLib::Ops::BVLShr ||
    op == SMTLib::Ops::BVULt ||
    op == SMTLib::Ops::BVULEq ||
    op == SMTLib::Ops::BVUGt ||
    op == SMTLib::Ops::BVUGEq ||
    op == SMTLib::Ops::BVSLt ||
    op == SMTLib::Ops::BVSLEq ||
    op == SMTLib::Ops::BVSGt ||
    op == SMTLib::Ops::BVSGEq ||
    op == SMTLib::Ops::Concat ||
    op == SMTLib::Ops::BVComp || 
	op == SMTLib::Ops::StrConcat
	
    >
    >{
    static constexpr  bool value = true; 
  };

  template<SMTLib::Ops,class T = void>
  struct is_unary {
    static constexpr bool value = false; 
  };
  
  template<SMTLib::Ops op>
  struct is_unary<op,std::enable_if_t<
		       op == SMTLib::Ops::Not ||
		       op == SMTLib::Ops::BVNot ||
		       op == SMTLib::Ops::BVNeg ||
		       op == SMTLib::Ops::BV2Bool ||
			   op == SMTLib::Ops::Bool2BV ||
					   op == SMTLib::Ops::StrLength 
			
		       
		       >
		  > {
    static constexpr bool value = true; 
  };
  
}

#endif
  
