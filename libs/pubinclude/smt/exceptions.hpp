#ifndef _SMT_EXCEPTION__
#define _SMT_EXCEPTION__

#include <stdexcept>

namespace SMTLib {
  class Exception : public std::runtime_error {
  public:
	Exception (const std::string& s) : std::runtime_error (s) {}
  };
  
  
  
}

#endif
