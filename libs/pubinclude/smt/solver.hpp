#ifndef _SOLVER__SMT
#define _SOLVER__SMT


#include <memory>
#include <vector>
#include <ostream>

#include <variant>
#include "smt/term.hpp"

namespace SMTLib {
  enum class Result {
	Satis,
	NSatis,
	Unknown
  };

  inline std::ostream& operator<< (std::ostream& os, Result r) {
    switch (r) {
	case Result::Satis:
	  return os << "Satis";
	case Result::NSatis:
	  return os << "NSatis";
	case Result::Unknown:
    default:
	  return os << "Unknown";
    }
    
  }

  using Assumptions = std::vector<Term_ptr>;

  using bitvector = std::vector<bool>;
  
  using Values = std::variant<std::int64_t,bool,string32,bitvector>;
  
  class Solver {
  public:
    virtual ~Solver () {}
    virtual void assert_formula (const Term_ptr& ) = 0;
    virtual Result check_sat () = 0;
    virtual Result check_sat (const Assumptions& ) = 0;
    virtual Values getModelValue (const Term_ptr&) const = 0; 
  };

  using Solver_ptr = std::unique_ptr<Solver>;
  
}

#endif
