#ifndef _TERM__
#define _TERM__

#include <sstream>
#include <ostream>
#include <memory>
#include <vector>
#include "smt/ops.hpp"
#include "smt/sort.hpp"


namespace SMTLib {
  using string32 = std::vector<char32_t>;
  

  class Term {
  public:
	Term ()  {}
    virtual ~Term () {}
    virtual std::size_t hash() const = 0;
    virtual Ops get_op() const = 0;
    virtual bool operator== (const Term& term) = 0;
    virtual std::ostream& output (std::ostream& ) const = 0;
    const std::string string_repr () const {
      std::stringstream str;
      this->output (str);
      return str.str();
    }
    operator std::string () const {
      return this->string_repr ();
    }
  };

  using Term_ptr = std::shared_ptr<Term>;
  inline std::ostream& operator<< (std::ostream& os, const Term& t) {
	return t.output (os);
  }
							
}

#endif
