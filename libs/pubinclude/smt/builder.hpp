#ifndef _BUILDER__SMT
#define _BUILDER__SMT


#include "smt/sort.hpp"
#include "smt/term.hpp"

#include <string>
#include <initializer_list>
#include <vector>

namespace SMTLib {

  
  class TermBuilder {
  public:
    virtual Sort_ptr makeBVSort (width_t) = 0;
    virtual Sort_ptr makeSort (SortKind, std::initializer_list<Sort_ptr> sorts) = 0;
    virtual Term_ptr buildTerm (Ops, std::initializer_list<Term_ptr> terms) = 0;
    virtual Term_ptr buildTerm (Ops, std::initializer_list<Term_ptr> terms, std::initializer_list<std::size_t> ops) = 0;
    virtual Term_ptr makeVar (Sort_ptr sort, const std::string name) = 0;
    virtual Term_ptr makeIntConst (std::int64_t) = 0;
    virtual Term_ptr makeBVIntConst (std::uint64_t, width_t width) = 0;
    virtual Term_ptr makeBoolConst (bool) = 0;
    virtual Term_ptr makeStringConst (const string32&) = 0;
    
  };
}


#endif
