#ifndef _CONTEXT__
#define _CONTEXT__

#include <memory>
#include <functional>
#include <iostream>

#include "term.hpp"
#include "builder.hpp"
#include "solver.hpp"

namespace SMTLib {
  class Context {
  public:
    virtual TermBuilder& getBuilder () = 0;
    virtual Solver_ptr makeSolver () = 0;    
  };

  using Context_ptr = std::shared_ptr<Context>; 

  using smt_create = std::function<Context_ptr()>;
  
  
  struct SMTBackendRegistrar;
  void registerSMT (const std::string&, SMTBackendRegistrar&);
  std::vector<SMTBackendRegistrar*> getSMTBackends ();
  SMTBackendRegistrar* getSMT (const std::string& s);
  
  
  struct SMTBackendRegistrar {
	SMTBackendRegistrar (const std::string& name,
			     smt_create func
			     ) : func(func),name(name) {
	  registerSMT (name,*this);
	}
	
	const std::string& getName () const {return name;}
	const std::string& getDescritpion () const {return descr;}
	smt_create getFunction () const {return func;}
	
	smt_create func;
        const std::string name;
	const std::string descr = "";
  };

  
}

#endif
