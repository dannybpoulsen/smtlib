#ifndef _TERM__
#define _TERM__

#include <memory>
#include "ops.hpp"

namespace SMTLib {
  enum class SortKind {
		       Bool
		       Integer,
		       Real,
		       BitVector,
		       Array,
			   String
  };

  class Sort {
  public:
    virtual SortKind getSortKind () const  =0;
    virtual bool operator== (const Sort&) const  = 0;
  };

  using width_t = std::uint64_t;
  using Sort_ptr = std:shared_ptr<Sort>;
  
}

#endif
